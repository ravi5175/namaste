cmake_minimum_required(VERSION 3.10)

project(namaste)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)


find_package(PkgConfig)
pkg_check_modules(GTKMM gtkmm-3.0 REQUIRED)

add_subdirectory(cmd)
